<?php
namespace Swiss\BestSeller\Block\Widget;

use Magento\Backend\Block\Template\Context;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\Template;
use Magento\Sales\Model\ResourceModel\Report\Bestsellers\Collection;
use Magento\Sales\Model\ResourceModel\Report\Bestsellers\CollectionFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Widget\Block\BlockInterface;

class BestSeller extends Template implements BlockInterface
{
    const DEFAULT_PRODUCT_LIMIT = 4;

    /**
     * @var string
     */
    protected $_template = 'product/widget/content/grid.phtml';

    /**
     * @var mixed
     */
    protected $_coreRegistry = null;
    /**
     * @var mixed
     */
    protected $_collectionFactory;

    /**
     * @var mixed
     */
    protected $_urlInterface;

    /**
     * @var int
     */
    private $_productLimit;

    /**
     * @var string Should contain the base url for media.
     */
    private $_mediaBaseUrl;

    /**
     * @var \Magento\Catalog\Model\ProductRepositoryInterface
     */
    private $_productRepositoryInterface;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Sales\Model\ResourceModel\Report\Bestsellers\CollectionFactory $collectionFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        CollectionFactory $collectionFactory,
        UrlInterface $urlInterface,
        ProductRepositoryInterface $productRepositoryInterface,
        array $data = []
    ) {

        if (!empty($data['limit'])) {
            $this->setLimit($data['limit']);
        }

        $this->_collectionFactory = $collectionFactory;
        $this->_coreRegistry      = $registry;
        $this->_urlInterface      = $urlInterface;
        $this->_productRepositoryInterface = $productRepositoryInterface;
        parent::__construct($context, $data);
    }

    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }
    /**
     * @return \Magento\Sales\Model\ResourceModel\Report\Bestsellers\Collection
     */
    public function getBestSellerData(): Collection
    {
        $collection = $this->_collectionFactory->create()->setModel(
            'Magento\Catalog\Model\Product'
        );
        $collection->setPageSize($this->getLimit());

        $collection->getSelect()->join(
            'catalog_category_product',
            'catalog_category_product.product_id ='
            . 'sales_bestsellers_aggregated_yearly.product_id',
            []
        );

        return $collection;
    }

    /**
     * @param $productId
     * @return mixed
     */
    public function getProductUrl($productId)
    {
        $product = $this->_productRepositoryInterface->getById($productId);
        return $product->getUrlModel()->getUrl($product);
    }

    /**
     * Retrieves and returns the base url for media.
     * Will return the public parameter if already set.
     *
     * @return string
     */
    public function getMediaBaseUrl(): string
    {
        if (!$this->_mediaBaseUrl) {
            $om                  = ObjectManager::getInstance();
            $storeManager        = $om->get(StoreManagerInterface::class);
            $currentStore        = $storeManager->getStore();
            $this->_mediaBaseUrl = $currentStore->getBaseUrl(
                UrlInterface::URL_TYPE_MEDIA
            );
        }

        return $this->_mediaBaseUrl;
    }

    /**
     * @return string
     */
    public function getAllProductsUrl(): string
    {
        return $this->_urlInterface->getUrl('all-products');
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        if ((int) $this->_productLimit > 0) {
            return $this->_productLimit;
        }

        return self::DEFAULT_PRODUCT_LIMIT;
    }

    /**
     * @param int $limit
     */
    public function setLimit(int $limit)
    {
        $this->_productLimit = (int) $limit;
    }

    /**
     * Calculates and returns the minimum and maximum prices for a gift-card.
     *
     * @param array $giftCardAmounts
     * @param float $minAmount
     * @param float $maxAmount
     * @param float $priceRate
     * @return array
     */
    public function getGiftCardPrices(
        $giftCardAmounts, $minAmount, $maxAmount, $priceRate
    ) {
        if ($giftCardAmounts === null) {
            $giftCardAmounts = [];
        }

        $minPrice = $minAmount * ($priceRate / 100);
        $maxPrice = $maxAmount * ($priceRate / 100);

        $priceRange = [];
        if ($minPrice > 0) {
            $priceRange[] = $minPrice;
        }
        if ($maxPrice > 0) {
            $priceRange[] = $maxPrice;
        }

        foreach ($giftCardAmounts as $gca) {
            $priceRange[] = $gca['price'];
        }

        sort($priceRange, SORT_NUMERIC);

        $minPrice = reset($priceRange);
        $maxPrice = end($priceRange);

        return [$minPrice, $maxPrice];
    }
}
